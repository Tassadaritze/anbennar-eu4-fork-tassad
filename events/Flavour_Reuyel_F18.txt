namespace = flavour_reuyel

#Special Election
country_event = {
    id = flavour_reuyel.1
    title = flavour_reuyel.1.t
    desc = flavour_reuyel.1.d
    picture = ELECTION_REPUBLICAN_eventPicture
    
    is_triggered_only = yes
    
    trigger = {
        has_reform = reuyeli_republic
        NOT = { has_country_flag = in_reuyel_election }
    }
    
    immediate = {
        hidden_effect = {
            set_country_flag = in_reuyel_election
			F18 = { save_event_target_as = reuyel_origin }
			set_saved_name = {
				key = bahari_name
				type = advisor
				scope = event_target:reuyel_origin
			}
			F17 = { save_event_target_as = crathanor_origin }
			set_saved_name = {
				key = crathanori_name
				type = advisor
				scope = event_target:crathanor_origin
			}
			F16 = { save_event_target_as = ourdia_origin }
			set_saved_name = {
				key = ourdi_name
				type = advisor
				scope = event_target:ourdia_origin
			}
			F15 = { save_event_target_as = marble_head_origin }
			set_saved_name = {
				key = goblin_name
				type = advisor
				scope = event_target:marble_head_origin
			}
			F37 = { save_event_target_as = evran_origin }
			set_saved_name = {
				key = sun_elf_name
				type = advisor
				scope = event_target:evran_origin
			}
			F23 = { save_event_target_as = tungr_origin }
			set_saved_name = {
				key = dwarf_name
				type = advisor
				scope = event_target:tungr_origin
			}
			F27 = { save_event_target_as = harpylen_origin }
			set_saved_name = {
				key = harpy_name
				type = advisor
				scope = event_target:harpylen_origin
			}
        }
    }
	
	after = {
		hidden_effect = {
			clear_saved_name = bahari_name
			clear_saved_name = crathanori_name
			clear_saved_name = ourdi_name
			clear_saved_name = goblin_name
			clear_saved_name = sun_elf_name
			clear_saved_name = dwarf_name
			clear_saved_name = harpy_name
            clr_country_flag = in_reuyel_election
        }
	}
	
	#Keep Current Ruler
	option = {
		name = flavour_reuyel.1.a
		trigger = { NOT = { months_of_ruling = 234 } } #Can't re-elect rulers after 20 years to prevent reelecting the same long lived dude
		ai_chance = { 
			factor = 60
			modifier = {
				factor = 0
				NOT = { republican_tradition = 25 }
			}	
			modifier = {
				factor = 0.5
				NOT = { republican_tradition = 50 }
			}				
			modifier = {
				factor = 0.5
				NOT = { republican_tradition = 75 }
			}	
			modifier = {
				factor = 2.0
				republican_tradition = 90
			}		
			
			#Anbennar
			modifier = {
				factor = 2.0
				ruler_has_personality = mage_personality
			}	
		}
		custom_tooltip = remains_ruler
		change_adm = 1
		change_dip = 1
		change_mil = 1
		if = {
			limit = {
				is_tribal = no 
				government = republic
				NOT = { republican_tradition = 20 }				
				NOT = { is_revolutionary_republic_trigger = yes }
				has_dlc = "Res Publica"
			}
			add_government_reform = presidential_despot_reform	
		}
		add_scaled_republican_tradition = -10
		random_list = {
			10 = {
				add_adm_power = 50
			}
			10 = {
				add_dip_power = 50
			}
			10 = {
				add_mil_power = 50
			}
		}
	}
	
	#Bahari Candidate
	option = {
		name = flavour_reuyel.1.b
		trigger = { accepted_culture = bahari }
		ai_chance = { factor = 20 }
		define_ruler = {
			name = bahari_name
			min_age = 30
			max_age = 50
			adm = 1
			dip = 1
			mil = 1
			max_random_adm = 3
			max_random_dip = 3
			max_random_mil = 3
			culture = bahari
		}
		small_increase_of_human_tolerance_effect = yes
	}
	
	#Crathanori Candidate
	option = {
		name = flavour_reuyel.1.c
		trigger = { accepted_culture = crathanori }
		ai_chance = { factor = 20 }
		define_ruler = {
			name = crathanori_name
			min_age = 30
			max_age = 50
			adm = 1
			dip = 1
			mil = 1
			max_random_adm = 3
			max_random_dip = 3
			max_random_mil = 3
			culture = crathanori
		}
		small_increase_of_human_tolerance_effect = yes
	}
	
	#Ourdi Candidate
	option = {
		name = flavour_reuyel.1.e
		trigger = { accepted_culture = ourdi }
		ai_chance = { factor = 20 }
		define_ruler = {
			name = ourdi_name
			min_age = 30
			max_age = 50
			adm = 1
			dip = 1
			mil = 1
			max_random_adm = 3
			max_random_dip = 3
			max_random_mil = 3
			culture = ourdi
		}
		small_increase_of_human_tolerance_effect = yes
	}
	
	#Goblin Candidate
	option = {
		name = flavour_reuyel.1.f
		trigger = { 
			accepted_culture = exodus_goblin
			religion = the_jadd
		}
		ai_chance = { factor = 20 }
		define_ruler = {
			name = goblin_name
			min_age = 30
			max_age = 50
			adm = 1
			dip = 1
			mil = 1
			max_random_adm = 3
			max_random_dip = 3
			max_random_mil = 3
			culture = exodus_goblin
		}
		small_increase_of_goblin_tolerance_effect = yes
	}
	
	#Sun Elf Candidate
	option = {
		name = flavour_reuyel.1.g
		trigger = { accepted_culture = sun_elf }
		ai_chance = { factor = 20 }
		define_ruler = {
			name = sun_elf_name
			min_age = 30
			max_age = 50
			adm = 1
			dip = 1
			mil = 1
			max_random_adm = 3
			max_random_dip = 3
			max_random_mil = 3
			culture = sun_elf
		}
		hidden_effect = { add_ruler_personality = immortal_personality }
		small_increase_of_elven_tolerance_effect = yes
	}
	
	#Dwarf Candidate
	option = {
		name = flavour_reuyel.1.h
		trigger = {
			accepted_culture = copper_dwarf
			religion = the_jadd
		}
		ai_chance = { factor = 20 }
		define_ruler = {
			name = dwarf_name
			min_age = 30
			max_age = 50
			adm = 1
			dip = 1
			mil = 1
			max_random_adm = 3
			max_random_dip = 3
			max_random_mil = 3
			culture = copper_dwarf
		}
		hidden_effect = { add_ruler_personality = immortal_personality }
		small_increase_of_dwarven_tolerance_effect = yes
	}
	
	#Harpy Candidate
	option = {
		name = flavour_reuyel.1.i
		trigger = {
			accepted_culture = firanyan_harpy
			religion = the_jadd
		}
		ai_chance = { factor = 20 }
		define_ruler = {
			name = harpy_name
			min_age = 30
			max_age = 50
			adm = 1
			dip = 1
			mil = 1
			max_random_adm = 3
			max_random_dip = 3
			max_random_mil = 3
			culture = firanyan_harpy
			female = yes
		}
		small_increase_of_harpy_tolerance_effect = yes
	}
}

#Death elections
country_event = {
    id = flavour_reuyel.2
    title = flavour_reuyel.2.t
    desc = flavour_reuyel.2.d
    picture = ELECTION_REPUBLICAN_eventPicture
    
    is_triggered_only = yes
    
    trigger = {
        has_reform = reuyeli_republic
        NOT = { has_country_flag = in_reuyel_election }
    }
    
    immediate = {
        hidden_effect = {
            set_country_flag = in_reuyel_election
			F18 = { save_event_target_as = reuyel_origin }
			set_saved_name = {
				key = bahari_name
				type = advisor
				scope = event_target:reuyel_origin
			}
			F17 = { save_event_target_as = crathanor_origin }
			set_saved_name = {
				key = crathanori_name
				type = advisor
				scope = event_target:crathanor_origin
			}
			F16 = { save_event_target_as = ourdia_origin }
			set_saved_name = {
				key = ourdi_name
				type = advisor
				scope = event_target:ourdia_origin
			}
			F15 = { save_event_target_as = marble_head_origin }
			set_saved_name = {
				key = goblin_name
				type = advisor
				scope = event_target:marble_head_origin
			}
			F37 = { save_event_target_as = evran_origin }
			set_saved_name = {
				key = sun_elf_name
				type = advisor
				scope = event_target:evran_origin
			}
			F23 = { save_event_target_as = tungr_origin }
			set_saved_name = {
				key = dwarf_name
				type = advisor
				scope = event_target:tungr_origin
			}
			F27 = { save_event_target_as = harpylen_origin }
			set_saved_name = {
				key = harpy_name
				type = advisor
				scope = event_target:harpylen_origin
			}
        }
    }
	
	after = {
		hidden_effect = {
			clear_saved_name = bahari_name
			clear_saved_name = crathanori_name
			clear_saved_name = ourdi_name
			clear_saved_name = goblin_name
			clear_saved_name = sun_elf_name
			clear_saved_name = dwarf_name
			clear_saved_name = harpy_name
            clr_country_flag = in_reuyel_election
        }
	}
	
	option = {
		name = flavour_reuyel.2.a
		trigger = { accepted_culture = bahari }
		ai_chance = { factor = 20 }
		define_ruler = {
			name = bahari_name
			min_age = 30
			max_age = 50
			adm = 1
			dip = 1
			mil = 1
			max_random_adm = 3
			max_random_dip = 3
			max_random_mil = 3
			culture = bahari
		}
		small_increase_of_human_tolerance_effect = yes
	}
	
	#Crathanori Candidate
	option = {
		name = flavour_reuyel.2.b
		trigger = { accepted_culture = crathanori }
		ai_chance = { factor = 20 }
		define_ruler = {
			name = crathanori_name
			min_age = 30
			max_age = 50
			adm = 1
			dip = 1
			mil = 1
			max_random_adm = 3
			max_random_dip = 3
			max_random_mil = 3
			culture = crathanori
		}
		small_increase_of_human_tolerance_effect = yes
	}
	
	#Ourdi Candidate
	option = {
		name = flavour_reuyel.2.c
		trigger = { accepted_culture = ourdi }
		ai_chance = { factor = 20 }
		define_ruler = {
			name = ourdi_name
			min_age = 30
			max_age = 50
			adm = 1
			dip = 1
			mil = 1
			max_random_adm = 3
			max_random_dip = 3
			max_random_mil = 3
			culture = ourdi
		}
		small_increase_of_human_tolerance_effect = yes
	}
	
	#Goblin Candidate
	option = {
		name = flavour_reuyel.2.e
		trigger = { 
			accepted_culture = exodus_goblin
			religion = the_jadd
		}
		ai_chance = { factor = 20 }
		define_ruler = {
			name = goblin_name
			min_age = 30
			max_age = 50
			adm = 1
			dip = 1
			mil = 1
			max_random_adm = 3
			max_random_dip = 3
			max_random_mil = 3
			culture = exodus_goblin
		}
		small_increase_of_goblin_tolerance_effect = yes
	}
	
	#Sun Elf Candidate
	option = {
		name = flavour_reuyel.2.f
		trigger = { accepted_culture = sun_elf }
		ai_chance = { factor = 20 }
		define_ruler = {
			name = sun_elf_name
			min_age = 30
			max_age = 50
			adm = 1
			dip = 1
			mil = 1
			max_random_adm = 3
			max_random_dip = 3
			max_random_mil = 3
			culture = sun_elf
		}
		hidden_effect = { add_ruler_personality = immortal_personality }
		small_increase_of_elven_tolerance_effect = yes
	}
	
	#Dwarf Candidate
	option = {
		name = flavour_reuyel.2.g
		trigger = {
			accepted_culture = copper_dwarf
			religion = the_jadd
		}
		ai_chance = { factor = 20 }
		define_ruler = {
			name = dwarf_name
			min_age = 30
			max_age = 50
			adm = 1
			dip = 1
			mil = 1
			max_random_adm = 3
			max_random_dip = 3
			max_random_mil = 3
			culture = copper_dwarf
		}
		hidden_effect = { add_ruler_personality = immortal_personality }
		small_increase_of_dwarven_tolerance_effect = yes
	}
	
	#Harpy Candidate
	option = {
		name = flavour_reuyel.2.h
		trigger = {
			accepted_culture = firanyan_harpy
			religion = the_jadd
		}
		ai_chance = { factor = 20 }
		define_ruler = {
			name = harpy_name
			min_age = 30
			max_age = 50
			adm = 1
			dip = 1
			mil = 1
			max_random_adm = 3
			max_random_dip = 3
			max_random_mil = 3
			culture = firanyan_harpy
			female = yes
		}
		small_increase_of_harpy_tolerance_effect = yes
	}
}
