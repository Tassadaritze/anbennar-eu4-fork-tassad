country_decisions = {
	construct_cliff_passage_1 = {
		major = yes
		potential = {
			OR = {
				owns_or_subject_of = 1876
				owns_or_subject_of = 2095
			}
			1808 = {
				NOT = { has_great_project = redrushes_climb }
				NOT = { has_construction = canal }
			}
		}
		
		provinces_to_highlight = {
			OR = {
				province_id = 1876
				province_id = 2095
			}
		}
		
		allow = {
			is_subject = no
			is_at_war = no
			treasury = 800
		}
		
		effect = {
			add_treasury = -800
			hidden_effect = {
				1808 = {
					add_great_project = redrushes_climb
				}
			}
				
			if = {
				limit = {
					owns_or_subject_of = 1876
				}
				1876 = {
					add_permanent_province_modifier = {
						name = building_ruin_cliff_passage
						duration = 1510
						hidden = no
					}
					set_province_flag = f_building_ruin_cliff_passage
					hidden_effect = { province_event = { id = CliffsOfRuin_events.1 days = 1510 } }
				}
			}
			else = {
				2095 = {
					add_permanent_province_modifier = { 
						name = building_ruin_cliff_passage
						duration = 1510
						hidden = no
					}
					set_province_flag = f_building_ruin_cliff_passage
					hidden_effect = { province_event = { id = CliffsOfRuin_events.1 days = 1510 } }
				}
			}
		}
		
		ai_will_do = {
			factor = 400
			modifier = { factor = 0 NOT = { treasury = 800 } }
		}
	}
	
	construct_cliff_passage_2 = {
		major = yes
		potential = {
			OR = {
				owns_or_subject_of = 1949
				owns_or_subject_of = 1810
			}
			1797 = {
				NOT = { has_great_project = spoorland_lift }
				NOT = { has_construction = canal }
			}
		}
		
		provinces_to_highlight = {
			OR = {
				province_id = 1949
				province_id = 1810
			}
		}
		
		allow = {
			is_subject = no
			is_at_war = no
			treasury = 800
		}
		
		effect = {
			add_treasury = -800
			hidden_effect = {
				1797 = {
					add_great_project = spoorland_lift
				}
			}
			
			if = {
				limit = {
					owns_or_subject_of = 1949
				}
				1949 = {
					add_permanent_province_modifier	 = {
						name = building_ruin_cliff_passage
						duration = 1510
						hidden = no
					}
					set_province_flag = f_building_ruin_cliff_passage
					hidden_effect = { province_event = { id = CliffsOfRuin_events.2 days = 1510 } }
				}
			}
			else = {
				1810 = {
					add_permanent_province_modifier	 = { 
						name = building_ruin_cliff_passage
						duration = 1510
						hidden = no
					}
					set_province_flag = f_building_ruin_cliff_passage
					hidden_effect = { province_event = { id = CliffsOfRuin_events.2 days = 1510 } }
				}
			}
		}
		
		ai_will_do = {
			factor = 400
			modifier = { factor = 0 NOT = { treasury = 800 } }
		}
	}
	
	construct_cliff_passage_3 = {
		major = yes
		potential = {
			OR = {
				owns_or_subject_of = 1042
				owns_or_subject_of = 1882
			}
			6510 = {
				NOT = { has_great_project = walkway_of_thorns }
				NOT = { has_construction = canal }
			}
		}
		
		provinces_to_highlight = {
			OR = {
				province_id = 1042
				province_id = 1882
			}
		}
		
		allow = {
			is_subject = no
			is_at_war = no
			treasury = 800
		}
		
		effect = {
			add_treasury = -800
			hidden_effect = {
				6510 = {
					add_great_project = walkway_of_thorns
				}
			}
			
			if = {
				limit = {
					owns_or_subject_of = 1042
				}
				1042 = {
					add_permanent_province_modifier	 = {
						name = building_ruin_cliff_passage
						duration = 1510
						hidden = no
					}
					set_province_flag = f_building_ruin_cliff_passage
					hidden_effect = { province_event = { id = CliffsOfRuin_events.3 days = 1510 } }
				}
			}
			else = {
				1882 = {
					add_permanent_province_modifier	 = { 
						name = building_ruin_cliff_passage
						duration = 1510
						hidden = no
					}
					set_province_flag = f_building_ruin_cliff_passage
					hidden_effect = { province_event = { id = CliffsOfRuin_events.3 days = 1510 } }
				}
			}
		}
		
		ai_will_do = {
			factor = 400
			modifier = { factor = 0 NOT = { treasury = 800 } }
			modifier = { factor = 0 religion = ynn_river_worship }
		}
	}
	
	construct_cliff_passage_4 = {
		major = yes
		potential = {
			OR = {
				owns_or_subject_of = 1901
				owns_or_subject_of = 1835
			}
			1783 = {
				NOT = { has_great_project = arca_noruin }
				NOT = { has_construction = canal }
			}
		}
		
		provinces_to_highlight = {
			OR = {
				province_id = 1901
				province_id = 1835
			}
		}
		
		allow = {
			is_subject = no
			is_at_war = no
			treasury = 800
		}
		
		effect = {
			add_treasury = -800
			hidden_effect = {
				1783 = {
					add_great_project = arca_noruin
				}
			}
			
			if = {
				limit = {
					owns_or_subject_of = 1901
				}
				1901 = {
					add_permanent_province_modifier	 = {
						name = building_ruin_cliff_passage
						duration = 1510
						hidden = no
					}
					set_province_flag = f_building_ruin_cliff_passage
					hidden_effect = { province_event = { id = CliffsOfRuin_events.4 days = 1510 } }
				}
			}
			else = {
				1835 = {
					add_permanent_province_modifier	 = { 
						name = building_ruin_cliff_passage
						duration = 1510
						hidden = no
					}
					set_province_flag = f_building_ruin_cliff_passage
					hidden_effect = { province_event = { id = CliffsOfRuin_events.4 days = 1510 } }
				}
			}
		}
		
		ai_will_do = {
			factor = 400
			modifier = { factor = 0 NOT = { treasury = 800 } }
			modifier = { factor = 0 religion = ynn_river_worship }
		}
	}
	
	construct_cliff_passage_5 = {
		major = yes
		potential = {
			OR = {
				owns_or_subject_of = 1031
				owns_or_subject_of = 2757
			}
			1801 = {
				NOT = { has_great_project = arca_venaan }
				NOT = { has_construction = canal }
			}
		}
		
		provinces_to_highlight = {
			OR = {
				province_id = 1031
				province_id = 2757
			}
		}
		
		allow = {
			is_subject = no
			is_at_war = no
			treasury = 800
		}
		
		effect = {
			add_treasury = -800
			hidden_effect = {
				1801 = {
					add_great_project = arca_venaan
				}
			}
			
			if = {
				limit = {
					owns_or_subject_of = 1031
				}
				1031 = {
					add_permanent_province_modifier = {
						name = building_ruin_cliff_passage
						duration = 1510
						hidden = no
					}
					set_province_flag = f_building_ruin_cliff_passage
					hidden_effect = { province_event = { id = CliffsOfRuin_events.5 days = 1510 } }
				}
			}
			else = {
				2757 = {
					add_permanent_province_modifier = { 
						name = building_ruin_cliff_passage
						duration = 1510
						hidden = no
					}
					set_province_flag = f_building_ruin_cliff_passage
					hidden_effect = { province_event = { id = CliffsOfRuin_events.5 days = 1510 } }
				}
			}
		}
		
		ai_will_do = {
			factor = 400
			modifier = { factor = 0 NOT = { treasury = 800 } }
			modifier = { factor = 0 religion = ynn_river_worship NOT = { tag = H27 } }
		}
	}

	construct_cliff_passage_6 = {
		major = yes
		potential = {
			OR = {
				owns_or_subject_of = 1128
				owns_or_subject_of = 1153
			}
			6502 = {
				NOT = { has_great_project = arbeloch_ascensor }
				NOT = { has_construction = canal }
			}
		}
		
		provinces_to_highlight = {
			OR = {
				province_id = 1128
				province_id = 1153
			}
		}
		
		allow = {
			is_subject = no
			is_at_war = no
			treasury = 800
		}
		
		effect = {
			add_treasury = -800
			hidden_effect = {
				6502 = {
					add_great_project = arbeloch_ascensor
				}
			}
			
			if = {
				limit = {
					owns_or_subject_of = 1128
				}
				1128 = {
					add_permanent_province_modifier = {
						name = building_ruin_cliff_passage
						duration = 1510
						hidden = no
					}
					set_province_flag = f_building_ruin_cliff_passage
					hidden_effect = { province_event = { id = CliffsOfRuin_events.6 days = 1510 } }
				}
			}
			else = {
				1153 = {
					add_permanent_province_modifier	= { 
						name = building_ruin_cliff_passage
						duration = 1510
						hidden = no
					}
					set_province_flag = f_building_ruin_cliff_passage
					hidden_effect = { province_event = { id = CliffsOfRuin_events.6 days = 1510 } }
				}
			}
		}
		
		ai_will_do = {
			factor = 400
			modifier = { factor = 0 NOT = { treasury = 800 } }
			modifier = { factor = 0 religion = ynn_river_worship }
		}
	}
	
	cancel_cliff_passage = {
		major = yes
		potential = {
			ai = no
			any_province = {
				has_province_modifier = building_ruin_cliff_passage
				country_or_subject_holds = ROOT
			}
			NOT = { has_country_flag = ruin_cliff_finished }
		}
		
		provinces_to_highlight = {
			has_province_modifier = building_ruin_cliff_passage
			country_or_subject_holds = ROOT
		}
		
		allow = {
			is_subject = no
			is_at_war = no
		}
		
		effect = {
			add_treasury = 800
			random_province = {
				limit = {
					has_province_modifier = building_ruin_cliff_passage
					country_or_subject_holds = ROOT
				}
				remove_province_modifier = building_ruin_cliff_passage
				clr_province_flag = f_building_ruin_cliff_passage
				if = {
					limit = {
						OR = {
							province_id = 2095
							province_id = 1876
						}
					}
					1808 = { cancel_construction = yes }
				}
				else_if = {
					limit = {
						OR = {
							province_id = 1810
							province_id = 1949
						}
					}
					1797 = { cancel_construction = yes }
				}
				else_if = {
					limit = {
						OR = {
							province_id = 1042
							province_id = 1882
						}
					}
					6510 = { cancel_construction = yes }
				}
				else_if = {
					limit = {
						OR = {
							province_id = 1901
							province_id = 1835
						}
					}
					1783 = { cancel_construction = yes }
				}
				else_if = {
					limit = {
						OR = {
							province_id = 2757
							province_id = 1031
						}
					}
					1801 = { cancel_construction = yes }
				}
				else_if = {
					limit = {
						OR = {
							province_id = 1153
							province_id = 1128
						}
					}
					6502 = { cancel_construction = yes }
				}
			}
		}
		
		ai_will_do = {
			factor = 0
		}
	}
}
